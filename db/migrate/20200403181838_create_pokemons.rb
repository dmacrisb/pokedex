class CreatePokemons < ActiveRecord::Migration[6.0]
  def change
    create_table :pokemons do |t|
      t.string :pokename
      t.string :poketype
      t.string :pokeregion
      t.string :pokesize

      t.timestamps
    end
  end
end
