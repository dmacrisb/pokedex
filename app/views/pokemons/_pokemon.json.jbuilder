json.extract! pokemon, :id, :pokename, :poketype, :pokeregion, :pokesize, :created_at, :updated_at
json.url pokemon_url(pokemon, format: :json)
